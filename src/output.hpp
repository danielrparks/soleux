#ifndef SOLEUXGPX
#define SOLEUXGPX

#include "RunRecord.hpp"
#include <fstream>
#include <vector>

namespace soleux {
	enum CSVCol { runnumber, lapnumber, start, end, distance, duration, calories, avgpace, maxpace, avgspeed, maxspeed, bpm };
	extern int offset;
	void gpx(std::map<int, GPSRecord>::iterator wpts, std::map<int, GPSRecord>::iterator wptend, std::ostream& out);
	void csv(std::map<int, RunRecord>::iterator runs, std::map<int, RunRecord>::iterator runend, std::ostream& out, std::vector<soleux::CSVCol>& columns, bool imperial); // set imperial to true if you want stupid units
	void csv(std::map<int, RunRecord>& runs, std::vector<int>& keys, std::ostream& out, std::vector<soleux::CSVCol>& columns, bool imperial);
	void csv(std::map<int, LapRecord>::iterator laps, std::map<int, LapRecord>::iterator lapend, int runnumber, std::ostream& out, std::vector<soleux::CSVCol>& columns, bool imperial); // set imperial to true if you want stupid units
	void raw(Record& rec, std::ostream& out);
	std::vector<std::string> split(std::string str, char delimiter);
	std::vector<soleux::CSVCol>::size_type indexOfCol(std::vector<soleux::CSVCol> cols, soleux::CSVCol col);
	std::vector<soleux::CSVCol> colsFromString(std::string str);
	std::pair<std::vector<soleux::CSVCol>, char> colsUnitsFromString(std::string str);
	void formatColumns(std::ostream& out, std::vector<soleux::CSVCol>& columns, bool imperial);
}

#endif
