#ifndef SOLEUXCOMLINUX
#define SOLEUXCOMLINUX
#include "Communicator.hpp"
//#include <string>

namespace soleux {
	class CommunicatorLinux : public Communicator {
	private:
		int portfd;
		std::string serialNumber;
	public:
		CommunicatorLinux();
		CommunicatorLinux(std::string devpath);
		CommunicatorLinux(const CommunicatorLinux& other) = delete; // you shouldn't have more than one accessing the same device, so it doesn't make sense to be able to copy them
		CommunicatorLinux& operator=(const CommunicatorLinux& other) = delete;
		CommunicatorLinux(CommunicatorLinux&& other) noexcept;
		CommunicatorLinux& operator=(CommunicatorLinux&& other) noexcept;
		std::string open(std::string devpath);
		void write(const std::array<char, 32>& buf);
		void read(std::array<char, 36>& buf);
		void close();
		static std::vector<std::unique_ptr<Communicator>> search();
		void sleepMS(int ms);
		std::string getSerialNumber();
		~CommunicatorLinux();
	};
}

#endif
