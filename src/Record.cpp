#include "Record.hpp"
using namespace soleux;

#include <algorithm>
#include <iterator>

Record::Record(std::array<char, 36>& buffer) : data(buffer) {}


Record::Type Record::determineType(std::array<char, 36>& buffer) {
	if (buffer[33] == 0 && buffer[35] == -3) {
		return run;
	}
	else if (buffer[33] != 0 && buffer[35] == -3) {
		return lap;
	}
	else if ((buffer[32] != 0 || buffer[33] != 0) && buffer[35] == -6) { // I'm not in the mood to check if the coordinates are properly transposed
		return gps;
	}
	else if (buffer[0] == -18 && buffer[1] == -18 && buffer[2] == -18 && buffer[3] == -18) {
		return eof;
	}
	else {
		return unset;
	}
}

int Record::getNumber() {
	return data[34] + 1; // adding 1 to match the number displayed on the watch
}

long Record::numberFromBytes(std::array<char, 36>::iterator start, int amt, bool isLittleEndian) {
	std::array<unsigned char, 36>::iterator fixed = reinterpret_cast<std::array<unsigned char, 36>::iterator>(start);
	long result = 0;
	if (isLittleEndian) {
		for (int i = 0; i < amt; i++) {
			result += (long)(*fixed) << 8 * i;
			fixed++;
		}
	}
	else {
		// for (int i = amt - 1; i >= 0; i--) {
		// 	result += buf[i] << 8 * (amt - i - 1);
		// }
		for (int i = 0; i < amt; i++) {
			result += (long)(*fixed) << 8 * (amt - i - 1);
			fixed++;
		}
	}
	return result;
}

long slowpow(long base, long exp) {
	long result = 1;
	for (long i = 0; i < exp; i++) {
		result *= base;
	}
	return result;
}

long Record::transposeBCD(std::array<unsigned char, 36>::iterator start, int len) {
	long result = 0;
	for (int i = 0; i < len; i++) {
		int leftbit = (*start & 0xf0) >> 4;
		int rightbit = *start & 0x0f;
		if (leftbit > 9 || rightbit > 9) return -1; // this is not transposed decimal after all
		result += (leftbit * 10 + rightbit) * slowpow(10, (len - i - 1) * 2);
		start++;
	}
	return result;
}

long Record::transposeBCD(std::array<char, 36>::iterator start, int len) {
	std::array<unsigned char, 36>::iterator fixed = reinterpret_cast<std::array<unsigned char, 36>::iterator>(start);
	return transposeBCD(fixed, len);
}

#ifdef DEBUG
void Record::printDesc(std::ostream& debugLog) {
	debugLog << "Record:\n";
	switch (determineType(data)) {
		case eof:
			debugLog << formatDesc(0, 4, "Ident-EOF") << '\n';
			break;
		case gps:
			debugLog << formatDesc(32, 33, "Ident-GPS") << '\n';
			debugLog << formatDesc(35, 35, "Ident-GPS") << '\n';
			break;
		case lap:
			debugLog << formatDesc(33, 33, "Ident-LAP") << '\n';
			debugLog << formatDesc(35, 35, "Ident-LAP") << '\n';
			break;
		case run:
			debugLog << formatDesc(33, 33, "Ident-RUN") << '\n';
			debugLog << formatDesc(35, 35, "Ident-RUN") << '\n';
			break;
		default:
			break;
	}
}
#endif
