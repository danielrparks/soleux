#include "Progress.hpp"
using namespace soleux;

Progress::Progress() {
	clear();
}

void Progress::clear() {
	stats = Stats();
	estimatedPercentage = 0;
	stage = not_receiving;
}

Progress::Stats::Stats() {
	recordsProcessed = 0;
	runRecordsProcessed = 0;
	lapRecordsProcessed = 0;
	gpsRecordsProcessed = 0;
	estimatedGPSRecords = 0;
	averageCentimetersPerGPSRecord = 0;
	totalDistance = 0;
	averageSamples = 0;
}

const double Progress::centimetersPerGPSRecord = 672.0407;
