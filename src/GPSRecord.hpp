#ifndef SOLEUXGPSREC
#define SOLEUXGPSREC

#include "Record.hpp"

#include <chrono>
#include <ctime>

namespace soleux {
	class GPSRecord : public Record {
	private:
		std::tm timetm;
	public:
		int segment;
		std::chrono::system_clock::time_point timestamp;
		long distance;
		double latitude;
		double longitude;
		int bpm;
		GPSRecord(std::array<char, 36>& buf);
		void updateTime(std::chrono::system_clock::time_point start);
#ifdef DEBUG
		void printDesc(std::ostream& debugLog);
#endif
	};
}

#endif
