#ifndef SOLEUXPROG
#define SOLEUXPROG

namespace soleux {
	class Progress {
	public:
		class Stats {
		public:
			long long recordsProcessed;
			int runRecordsProcessed;
			int lapRecordsProcessed;
			long long gpsRecordsProcessed;
			long long estimatedGPSRecords;
			double averageCentimetersPerGPSRecord;
			double averageSamples;
			long long totalDistance;
			Stats();
		};
		enum Stage { not_receiving, runs, laps, wpts, done };
		double estimatedPercentage;
		Stage stage;
		Stats stats;
		Progress();
		void clear();
		static const double centimetersPerGPSRecord; // determined empirically
	};
}

#endif
