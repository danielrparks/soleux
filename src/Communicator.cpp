#include "Communicator.hpp"
#include "Record.hpp"
#include <sstream>
#include <iomanip>

using namespace soleux;

const std::array<char, 32> Communicator::startdata {8, 8, -86, -86, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
const std::array<char, 32> Communicator::unlink {-1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
const std::array<char, 32> Communicator::link {-18, -18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
const std::array<char, 32> Communicator::deleterecord {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3};
const long Communicator::sleeptime = 500; // one half second
const long Communicator::deletetime = 1000; // one whole second! because nobody bothered to find out how long it actually takes for the watch to delete a record

Communicator::Communicator() : path("") {}

void Communicator::deleteRun(int runNumber) {
	std::array<char, 32> toDelete(Communicator::deleterecord);
	toDelete[30] = runNumber - 1;
	// the numbers returned by this library are 1-indexed because that is how the user interface of the watch displays them
	// internally, they are stored zero-indexed
	// I don't like this, but I think it is best
	write(toDelete);
	sleepMS(deletetime);
}

void Communicator::start() {
	write(startdata);
}

void Communicator::stop() {
	try {
		write(unlink);
	} catch (...) {
		// ignore
	}
}

std::string Communicator::linkAndCheck() {
	write(unlink);
	sleepMS(sleeptime);
	write(link);
	std::array<char, 36> handshake;
	handshake.fill(0);
	read(handshake);
	// long serialNumber = Record::transposeBCD(handshake.begin(), 7); // actually this is not necessarily bcd
	long serialNumber = Record::numberFromBytes(handshake.begin(), 7, false);
	std::stringstream result;
	result << std::hex << serialNumber;
	// return std::string(handshake.begin() + 7, handshake.begin() + 27); // This is not the serial number. I don't know what it is.
	return result.str();
}


not_soleus_error::not_soleus_error(std::string& device_identifier) : errmsg("Not a Soleus device: " + device_identifier) {}
const char* not_soleus_error::what() {
	return errmsg.c_str();
}
