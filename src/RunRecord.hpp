#ifndef SOLEUXRUNREC
#define SOLEUXRUNREC

#include "LapRecord.hpp"
#include "GPSRecord.hpp"

#include <chrono>
#include <map>

namespace soleux {
class RunRecord : public Record {
	public:
		RunRecord(std::array<char, 36>& buf);
		void rectify();
		std::chrono::system_clock::time_point begin;
		std::chrono::system_clock::time_point end;
		long distance; // centimeters
		long calories;
		int maxspeed; // km/hr * 10
		double averageBPM;
		std::map<int, LapRecord> laps;
		std::map<int, GPSRecord> wpts; // waypoints
#ifdef DEBUG
		void printDesc(std::ostream& debugLog);
#endif
	};
}

#endif
