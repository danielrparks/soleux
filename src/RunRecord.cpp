#include "RunRecord.hpp"
using namespace soleux;

#include <chrono>
#include <ctime>
#include <cmath>

RunRecord::RunRecord(std::array<char, 36>& buf) : Record(buf) {
	std::tm dtime;
	dtime.tm_isdst = -1;
	dtime.tm_year = 100 + buf[5];
	dtime.tm_mon = buf[4] - 1;
	dtime.tm_mday = buf[3];
	dtime.tm_hour = buf[2];
	dtime.tm_min = buf[1];
	dtime.tm_sec = buf[0];
	begin = std::chrono::system_clock::from_time_t(std::mktime(&dtime));
	dtime.tm_hour = buf[27];
	dtime.tm_min = buf[26]; // this value is temporary until it can be replaced by the data from the lap, as it does not include seconds
	dtime.tm_sec = 0;
	end = std::chrono::system_clock::from_time_t(std::mktime(&dtime));
	maxspeed = 0; // will be rectified later
}

void RunRecord::rectify() {
	if (laps.count(1) == 0 && ! laps.empty()) { // all of the data is wrong if we cannot find the first element, but gaps elsewhere will only cause minor inaccuracies
		laps.clear();
	}
	else if (! laps.empty()) {
		auto it = laps.begin();
		it->second.begin = begin;
		it->second.updateTime(begin, begin);
		std::chrono::system_clock::time_point lasttime = it->second.end;
		long lastdistance = it->second.distance;
		long lastcalories = it->second.calories;
		if (it->second.maxspeed > maxspeed) maxspeed = it->second.maxspeed;
		it++;
		for (; it != laps.end(); it++) {
			long tempdistance = it->second.distance;
			it->second.distance -= lastdistance;
			lastdistance = tempdistance;
			long tempcalories = it->second.calories;
			it->second.calories -= lastcalories;
			lastcalories = tempcalories;
			it->second.begin = lasttime;
			it->second.updateTime(begin, lasttime);
			lasttime = it->second.end;
			if (it->second.maxspeed > maxspeed) maxspeed = it->second.maxspeed;
		}
		if (std::abs(std::chrono::duration_cast<std::chrono::seconds>(lasttime - end).count()) <= 60) {
			// this is close enough that we should correct it
			// if it were farther off, some data would be missing and we should leave it for the user to correct
			end = lasttime;
		}
		distance = lastdistance;
		calories = lastcalories;
	}
	if (! wpts.empty()) {
		unsigned long long bpmsum = 0;
		unsigned long bpmreadings = 0;
		auto it = wpts.begin();
		it->second.updateTime(begin);
		std::chrono::system_clock::time_point lasttime = it->second.timestamp;
		it++;
		for (; it != wpts.end(); it++) {
			it->second.updateTime(lasttime);
			lasttime = it->second.timestamp;
			bpmsum += it->second.bpm;
			bpmreadings++;
		}
		averageBPM = (double) bpmsum / bpmreadings;
	}
}


#ifdef DEBUG
void RunRecord::printDesc(std::ostream& debugLog) {
	Record::printDesc(debugLog);
	debugLog << "RunRecord:\n";
	std::time_t temp_time_t = std::chrono::system_clock::to_time_t(begin);
	debugLog << formatDesc(0, 5, "Run SMHDMY", std::put_time(std::localtime(&temp_time_t), "%c")) << '\n';
	temp_time_t = std::chrono::system_clock::to_time_t(end);
	debugLog << formatDesc(26, 27, "Run MH", std::put_time(std::localtime(&temp_time_t), "%c")) << '\n';
}
#endif
