#ifndef SOLEUXLAPREC
#define SOLEUXLAPREC

#include "Record.hpp"

#include <chrono>
#include <ctime>

namespace soleux {
	class LapRecord : public Record {
	private:
		std::tm endtm;
		char centisec;
	public:
		long distance; // most fields are intentionally public for easy access, but be aware that changing them won't change the data
		long calories; // calories * 10 ^ 4
		int maxspeed; // km/hr * 10
		int segment;
		std::chrono::system_clock::time_point begin;
		std::chrono::system_clock::time_point end;
		LapRecord(std::array<char, 36>& buf);
		void updateTime(std::chrono::system_clock::time_point start, std::chrono::system_clock::time_point previous);
#ifdef DEBUG
		void printDesc(std::ostream& debugLog);
#endif
	};
}

#endif
