#ifndef SOLEUXCOM
#define SOLEUXCOM
#include <string>
#include <vector>
#include <array>
#include <memory>
#include <exception>

namespace soleux {
	class Communicator {
	public:
		std::string path;
		Communicator();
		virtual ~Communicator() = default;
		virtual void write(const std::array<char, 32>& buf) = 0;
		virtual void read(std::array<char, 36>& buf) = 0;
		virtual void close() = 0;
		virtual void sleepMS(int ms) = 0;
		virtual std::string getSerialNumber() = 0;
		// subclasses must also implement:
		// static std::vector<std::unique_ptr<Communicator>> search() = 0;
		void deleteRun(int runNumber);
		void start();
		void stop();
		static const long sleeptime;
		static const long deletetime;
		static const std::array<char, 32> startdata;
		static const std::array<char, 32> unlink;
		static const std::array<char, 32> link;
		static const std::array<char, 32> deleterecord;
	protected:
		std::string linkAndCheck();
	};
	class not_soleus_error : public std::exception {
		using std::exception::what;
	private:
		std::string errmsg;
	public:
		not_soleus_error(std::string& device_identifier);
		const char* what();
	};
}

#endif
