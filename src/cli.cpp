#include "Soleux.hpp"
#include "output.hpp"
#include "CommunicatorLinux.hpp"
#include "unistd.h" // for getopt
#include "getopt.h"
#include <iomanip>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <random>
#include <stdexcept>
#include <algorithm>

#define USAGE std::cerr << usage;\
return 1
#define ERROR std::cerr << argv[0] << ": " <<
#define WARNING std::cerr << argv[0] << ": WARNING: " <<

std::string usage(
	"Options:\n"
	"  --prefix, -p <prefix> - prefix of gpx files\n"
	"  --csv, -o <filename> - name of csv file (with extension)\n"
	"  --run-columns, -c <column_list> - columns in csv file for runs\n"
	"  --lap-columns, -e <column_list> - columns in csv file for laps\n"
	"  --download, -d [record_list] - download some or all records from the device\n"
	"  --delete, -r [record_list] - delete some or all records from the device.\n"
	"    If combined with --download, this action will be performed AFTER downloading\n"
	"    files from the device :)\n"
	"  --detect-device, -f - attempt to find the device instead of specifying it\n"
	"    WARNING: This option may cause soleux to write to arbitrary serial devices\n"
	"    on your system!\n"
	"  --offset, -t <offset> - write run number n as n+<offset> instead\n"
	"  --append, -a - attempt to automatically detect the offset, and insert the new\n"
	"    records into <filename>. If you used non-default values for <prefix>, \n"
	"    <filename>, or --imperial, you should specify those for this command too.\n"
	"    Column orders and offset will be autodetected from the file, and thus if\n"
	"    you specify those on the command line they will be ignored.\n"
	"  --imperial, -i - use imperial units instead of metric\n"
#ifdef DEBUG
	"  --debug, -b - write out debugging data\n"
#endif
	"  --version, -V - print the version of the program and library\n"
	"  [device] - string describing the device on your operating system,\n"
	"    e.g. `/dev/ttyUSB0` for linux\n"
	"\n"
	"Where:\n"
	"  <prefix> represents the part of the filename without an extension\n"
	"    (the run number and the extension `.gpx` will be appended to the prefix)\n"
	"  <filename> represents the entire name of the file to which run and lap data\n"
	"    will be written in CSV format\n"
	"  <column_list> is a list of columns that will be included in the CSV output,\n"
	"    delimited by commas. The default is `Run #,Start Time,End Time,Duration,\n"
	"    Distance,Calories,Heart Rate` for runs and `Run #,Lap #,Start Time,End Time,\n"
	"    Duration,Distance,Max. Speed,Calories` for laps. Note that you may need\n"
	"    to quote the string to prevent your shell from expanding the spaces into\n"
	"    multiple arguments.\n"
	"    The supported columns are `Run #,Lap #,Start Time,End Time,Distance,\n"
	"    Duration,Calories,Avg. Pace,Max. Pace,Avg. Speed,Max. Speed,Heart Rate`\n"
	"    Note that `Lap #` is dual meaning; for laps, it is the sequential\n"
	"    identifier of the lap, and for runs, it is how many laps were recorded\n"
	"    for that run.\n"
	"    Also note that heart rate data is not recorded for individual laps.\n"
	"  [record_list] is a list of record numbers in the range [1, 30], delimited\n"
	"    by commas or dashes for ranges (i.e. `1,3,5,6-10`)\n"
	"    record_list must be specified immediately after the option to which it\n"
	"    pertains, i.e. -d1,2,3 or --delete=4,5,6\n"
);

struct option longopts[] = {
	{ "prefix", 1, NULL, 'p' },
	{ "csv", 1, NULL, 'o' },
	{ "run-columns", 1, NULL, 'c' },
	{ "lap-columns", 1, NULL, 'e' },
	{ "download", 2, NULL, 'd' },
	{ "delete", 2, NULL, 'r' },
	{ "detect-device", 0, NULL, 'f' },
	{ "imperial", 0, NULL, 'i' },
	{ "version", 0, NULL, 'V'},
	{ "offset", 1, NULL, 't'},
	{ "append", 0, NULL, 'a'},
#ifdef DEBUG
	{ "debug", 0, NULL, 'b' },
#endif
	{NULL, 0, NULL, 0} // terminator
};

// bool validateRunList(std::string list) {
// 	try {
// 		for (std::string& s: Soleux::split(list, ',')) {
// 			int i = stoi(s);
// 			if (i < 1 || i > 30) return false;
// 		}
// 	}
// 	catch (...) {
// 		return false;
// 	}
// 	return true;
// }
//
// std::vector<std::string> split(std::string str, char delimiter) {
// 	std::vector<std::string> result;
// 	unsigned int lastind = 0;
// 	for (unsigned int i = 0; i < str.size(); i++) {
// 		if (str[i] == delimiter) {
// 			result.push_back(str.substr(lastind, i - lastind));
// 			lastind = i + 1;
// 		}
// 	}
// 	if (lastind != str.size()) {
// 		result.push_back(str.substr(lastind, str.size() - lastind));
// 	}
// 	return result;
// }

std::vector<int> populateRange(std::string range) {
	std::vector<int> result;
	if (range.size() == 0) return result;
	int last = -1;
	for (std::string::size_type i = 0; i < range.size(); i++) {
		if (range[i] >= '0' && range[i] <= '9') {
			if (last == -1) last = range[i] - '0';
			else last = last * 10 + range[i] - '0';
		}
		else if (range[i] == ',') {
			result.push_back(last);
			last = -1;
		}
		else if (range[i] == '-') {
			int next = -1;
			i++;
			for (; i < range.size() && range[i] >= '0' && range[i] <= '9'; i++) {
				if (next == -1) next = range[i] - '0';
				else next = next * 10 + range[i] - '0';
			}
			if (last == -1 || next == -1 || (i != range.size() && range[i] != ',')) {
				throw std::invalid_argument("invalid range");
			}
			i++; // this could go one past the end of the vector! Here that's fine, but take note!
			for (int j = last; j <= next; j++) {
				result.push_back(j);
			}
			last = -1;
		}
		else if (range[i] == ' ' || range[i] == '\t' || range[i] == '\n') { // non-exhaustive list
			continue;
		}
		else {
			throw std::invalid_argument((std::string)("invalid character in range: ") + range[i]);
		}
	}
	if (last != -1) result.push_back(last);
	return result;
}

std::filesystem::path tmpf(std::filesystem::path dir, std::string prefix) {
		// this is not atomic but I couldn't find any way to do it both atomically and portably
		std::filesystem::path f;
		std::uint_fast64_t seed = 0;
		std::ofstream create;
		for (std::string::size_type i = 0; i < prefix.size() && i < 8; i++) {
			seed |= (uint_fast64_t)(prefix[i]) << 8 * i;
		}
		std::mt19937_64 r(seed);
		do {
			f = dir / (prefix + '~' + std::to_string(r()));
		} while (std::filesystem::exists(f));
		create.open(f);
		create.close();
		return f;
}

int main(int argc, char** argv) {
	int opt;
	std::filesystem::path prefix("run");
	std::filesystem::path csv("runs.csv");
	std::string csv_runheader("Run #,Start Time,End Time,Duration,Distance,Calories,Heart Rate");
	std::string csv_lapheader("Run #,Lap #,Start Time,End Time,Duration,Distance,Max. Speed,Calories");
	std::filesystem::path csv_old;
	bool download = false;
	std::string toDownloads("1-30");
	std::vector<int> toDownload;
	bool deleteRec = false;
	std::string toDeletes("1-30");
	std::vector<int> toDelete;
	bool detect = false;
	bool imperial = false;
	bool append = false;
#ifdef DEBUG
	bool debug = false;
	while ((opt = getopt_long(argc, argv, "p:o:c:e:d::r::fibVat:", longopts, NULL)) != -1) {
#else
	while ((opt = getopt_long(argc, argv, "p:o:c:e:d::r::fiVat:", longopts, NULL)) != -1) {
#endif
		switch (opt) {
			case 'p':
				prefix = std::string(optarg);
				break;
			case 'o':
				csv = std::string(optarg);
				break;
			case 'c':
				csv_runheader = std::string(optarg);
				break;
			case 'e':
				csv_lapheader = std::string(optarg);
				break;
			case 'd':
				download = true;
				if (optarg != 0) toDownloads = std::string(optarg);
				break;
			case 'r':
				deleteRec = true;
				if (optarg != 0) toDeletes = std::string(optarg);
				break;
			case 'f':
				detect = true;
				break;
			case 'i':
				imperial = true;
				break;
			case 'V':
				std::cout << "soleux version " << soleux::Soleux::versionMajor << '.' << soleux::Soleux::versionMinor << '.' << soleux::Soleux::versionPatch << '\n';
				return 0;
			case 'a':
				append = true;
				break;
			case 't':
				soleux::offset = std::stoi(std::string(optarg));
				break;
#ifdef DEBUG
			case 'b':
				debug = true;
				break;
#endif
			default:
				USAGE;
				break;
		}
	}
	try {
		toDownload = populateRange(toDownloads);
		toDelete = populateRange(toDeletes);
	}
	catch (const std::invalid_argument& e) {
		ERROR "Invalid run list: " << e.what() << '\n';
		USAGE;
	}
	for (int i: toDownload) {
		if (i < 1 || i > 30) {
			ERROR "Invalid run number " << i << '\n';
			ERROR "Run numbers must be between 1 and 30";
			USAGE;
		}
	}
	std::vector<soleux::CSVCol> runcols, lapcols;
	try {
		runcols = soleux::colsFromString(csv_runheader);
		lapcols = soleux::colsFromString(csv_lapheader);
	}
	catch (std::out_of_range& e) {
		ERROR "Invalid column list: " << e.what() << '\n';
		USAGE;
	}
	if (!download && !deleteRec) {
		ERROR "No action specified\n";
		USAGE;
	}
	std::unique_ptr<soleux::Communicator> initDev;
	if (detect) {
		if (optind < argc) {
			ERROR "Too many arguments\n";
			USAGE;
		}
		auto devices = soleux::CommunicatorLinux::search();
		if (devices.size() > 1) {
			ERROR "Multiple devices available: ";
			for (unsigned long i = 0; i < devices.size(); i++) {
				std::cerr << devices[i]->path << " ";
			}
			std::cerr << '\n';
			return 1;
		}
		else if (devices.size() == 0) {
			ERROR "No devices available\n";
			return 1;
		}
		else {
			initDev = std::move(devices[0]); // we have to move out of here since it's a unique ptr
			// it's fine since devices is about to go out of scope anyway
		}
	}
	else if (optind > argc - 1) {
		ERROR "Expected device argument\n";
		USAGE;
	}
	else if (optind < argc - 1) {
		ERROR "Too many arguments\n";
		USAGE;
	}
	else {
		initDev = std::make_unique<soleux::CommunicatorLinux>(soleux::CommunicatorLinux(argv[optind]));
	}
	std::cout << "Got watch " << initDev->getSerialNumber() << '\n';
#ifdef DEBUG
	soleux::Soleux watch(*initDev, debug ? "watch_debug.bin" : "", debug ? "watch_debug.log" : "");
#else
	soleux::Soleux watch(*initDev);
#endif
	if (download) {
		int initPrecision = std::cout.precision();
		std::cout.precision(2);
		std::cout << std::fixed;
		watch.start();
		std::cout << "Reading data... ~" << std::setw(5) << 0.0 << '%' << std::flush;
		const soleux::Progress& prog = watch.readDataBlock();
		while (prog.stage != soleux::Progress::Stage::done) {
			watch.readDataBlock();
			std::cout << "\b\b\b\b\b\b\b" << std::setw(6) << prog.estimatedPercentage << '%' << std::flush;
		}
		std::cout << '\n';
		std::cout.precision(initPrecision);
		std::cout << std::defaultfloat;
		// std::cout << "Stats: " << prog.stats.recordsProcessed << " records\n"
		//           << "       " << prog.stats.runRecordsProcessed << "run records\n"
		// 					<< "       " << prog.stats.lapRecordsProcessed << "lap records\n"
		// 					<< "       " << prog.stats.gpsRecordsProcessed << "gps records\n";
		watch.rectify();
		std::ofstream out;
		std::ifstream in;
		if (append) {
			// get the next number from runs
			std::filesystem::path rundir = prefix;
			std::filesystem::path runname = rundir.filename();
			rundir.remove_filename();
			if (rundir.empty()) rundir = "./";
			int thisoffset = 1;
			while (std::filesystem::exists((std::string) prefix + std::to_string(thisoffset) + ".gpx")) {
				thisoffset++;
			}
			thisoffset--;
			if (thisoffset > soleux::offset) soleux::offset = thisoffset;
			
			// get the run header and next number from the input file
			csv_old = csv;
			std::filesystem::path csvdir(csv);
			std::string csv_prefix = csvdir.filename();
			csvdir.remove_filename();
			if (csvdir.empty()) csvdir = "./";
			csv = tmpf(csvdir, csv_prefix);
			out.open(csv);
			in.open(csv_old);
			std::string origHeader;
			std::getline(in, origHeader);
			try {
				auto pair = soleux::colsUnitsFromString(origHeader);
				runcols = pair.first;
				if (pair.second & 1) imperial = (bool)(pair.second & 2);
			}
			catch (const std::out_of_range& e) {
				ERROR "Invalid run column list in file " << csv_old << ": " << e.what() << '\n';
				return 1;
			}
			out << origHeader << '\n';
			size_t numIndex = soleux::indexOfCol(runcols, soleux::CSVCol::runnumber);
			thisoffset = 1;
			std::string line;
			std::getline(in, line);
			bool checking = numIndex != (size_t) -1;
			while (line.find(',') != std::string::npos) {
				out << line << '\n';
				if (checking) {
					std::vector<std::string> splitted = soleux::split(line, ',');
					int thisnum = stoi(splitted[numIndex]);
					if (thisnum > thisoffset) thisoffset = thisnum;
				}
				std::getline(in, line);
			}
			if (thisoffset > soleux::offset) soleux::offset = thisoffset;
		} /* if (append) */
		else {
			out.open(csv);
			soleux::formatColumns(out, runcols, imperial);
		}
		std::ofstream out2;
		//out << soleux::csv_runheader;
		soleux::csv(watch.runs, toDownload, out, runcols, imperial);
		//out << "\n\nLaps\n" << soleux::csv_lapheader;
		if (append) {
			std::string line;
			while (line.find(',') == std::string::npos) {
				out << line << '\n';
				if (!std::getline(in, line)) std::cerr << "Read failed!\n";
			}
			try {
				auto pair = soleux::colsUnitsFromString(line);
				lapcols = pair.first;
				if (pair.second & 1) imperial = (bool)(pair.second & 2);
			}
			catch (const std::out_of_range& e) {
				ERROR "Invalid lap column list in file " << csv_old << ": " << e.what() << '\n';
				WARNING "Partial data left in: " << csv << '\n';
				return 1;
			}
			while (std::getline(in, line)) {
				out << line << '\n';
			}
			in.close();
		}
		else {
			out << "\n\nLaps\n";
			soleux::formatColumns(out, lapcols, imperial);
		}
		// for (auto it = watch.runs.begin(); it != watch.runs.end(); it++) {
		// 	soleux::csv(it->second.laps.begin(), it->second.laps.end(), it->first, out, lapcols, imperial);
		// 	if (it->second.wpts.size() > 0) {
		// 		out2.open(prefix + std::to_string(it->first) + ".gpx");
		// 		soleux::gpx(it->second.wpts.begin(), it->second.wpts.end(), out2);
		// 		out2.close();
		// 	}
		// }
		for (int i: toDownload) {
			if (watch.runs.count(i) < 1) continue;
			soleux::RunRecord& run = watch.runs.at(i);
			soleux::csv(run.laps.begin(), run.laps.end(), i, out, lapcols, imperial);
			if (run.wpts.size() > 0) {
				out2.open((std::string) prefix + std::to_string(i + soleux::offset) + ".gpx");
				soleux::gpx(run.wpts.begin(), run.wpts.end(), out2);
				out2.close();
			}
		}
		out.close();
		if (append) {
			try {
				std::filesystem::rename(csv, csv_old);
			}
			catch (const std::filesystem::filesystem_error& e) {
				ERROR e.what() << '\n';
				WARNING "Partial data left in: " << csv << '\n';
			}
		}
	} /* if (download) */
	if (deleteRec) {
		std::cout << "Deleted run   ";
		std::sort(toDelete.rbegin(), toDelete.rend()); // deletes in reverse order to avoid index shifting complications
		for (int i: toDelete) {
			watch.deleteRun(i);
			std::cout << "\b\b" << std::setw(2) << i << std::flush;
		}
		std::cout << '\n';
	}
	watch.stop();
	watch.close();
	return 0;
}
