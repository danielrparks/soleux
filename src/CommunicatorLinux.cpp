#include "CommunicatorLinux.hpp"
using namespace soleux;

#include <iostream>
#include <fstream>
#include <utility>
#include <filesystem> // yay! I can use this because soleux uses c++17!
#include <system_error>
// c headers

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>


const auto cread = read;
const auto cwrite = write;
const auto cclose = close;
const auto copen = open;
// look, I REALLY want to name my functions these things

CommunicatorLinux::CommunicatorLinux(std::string devpath) {
	open(devpath);
}

CommunicatorLinux::CommunicatorLinux()
: portfd(-1), serialNumber("") {}

std::string CommunicatorLinux::open(std::string devpath) {
	std::filesystem::path devpath2(devpath);
	// this device is likely a soleus device if manufacturer == "Silicon Labs" and product startswith CP210
	std::filesystem::path usbinfo = std::filesystem::canonical("/sys/class/tty/" + (std::string) devpath2.filename() + "/../../../..");
	std::string manufacturer, product;
	std::ifstream usbinfo2;
	usbinfo2.open((std::string) usbinfo + "/product");
	std::getline(usbinfo2, product);
	usbinfo2.close();
	usbinfo2.open((std::string) usbinfo + "/manufacturer");
	std::getline(usbinfo2, manufacturer);
	usbinfo2.close();
	if (manufacturer != "Silicon Labs" || product.find("CP210") != 0) {
		// not a soleus device, throw exception instead of constructing
		throw not_soleus_error(devpath);
	}
	
	portfd = copen(devpath.c_str(), O_RDWR);
	if (portfd < 0) {
		throw std::system_error(errno, std::system_category());
	}
	struct termios tty;
	memset(&tty, 0, sizeof tty);
	if (tcgetattr(portfd, &tty) != 0) {
		// these three exceptions should be non-fatal, but should issue a warning in case no devices are found
		throw std::system_error(errno, std::system_category());
	}
	tty.c_cflag &= ~PARENB; // no parity
	tty.c_cflag &= ~CSTOPB; // no stop bit
	tty.c_cflag |= CS8; // 8 data bits
	// note: the serialio code has it set to 3. But 3 means 8, apparently.
	tty.c_cflag &= ~CRTSCTS; // hardware flow control is enabled?
	tty.c_cflag |= CREAD | CLOCAL;
	tty.c_lflag &= ~ICANON; // recieve bytes not lines
	tty.c_lflag &= ~ECHO & ~ECHOE & ~ECHONL;
	tty.c_lflag &= ~ISIG; // disable signal codes
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // no software flow control
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // no special handling
	tty.c_oflag &= ~OPOST & ~ONLCR; // send newlines literally
	tty.c_cc[VTIME] = 100; // time out after ten seconds
	tty.c_cc[VMIN] = 0;
	cfsetispeed(&tty, B115200); // 115200 baud
	if (tcsetattr(portfd, TCSANOW, &tty) != 0) {
		throw std::system_error(errno, std::system_category());
	}
	path = devpath;
	serialNumber = linkAndCheck();
	return serialNumber;
}

CommunicatorLinux::CommunicatorLinux(CommunicatorLinux&& other) noexcept
: portfd(std::exchange(other.portfd, -1)), serialNumber(std::exchange(other.serialNumber, "")) {}

CommunicatorLinux& CommunicatorLinux::operator=(CommunicatorLinux&& other) noexcept {
	std::swap(portfd, other.portfd);
	std::swap(serialNumber, other.serialNumber);
	return *this;
}

void CommunicatorLinux::sleepMS(int ms) {
	usleep(ms * 1000);
}

void CommunicatorLinux::write(const std::array<char, 32>& buf) {
	int result = cwrite(portfd, buf.data(), buf.size());
	if (result != (signed long long) buf.size()) {
		if (result < 0) {
			throw std::system_error(errno, std::system_category());
		}
		else {
			throw std::system_error(std::make_error_code(std::errc::io_error), "write did not finish");
		}
	}
}

void CommunicatorLinux::read(std::array<char, 36>& buf) {
	int result = cread(portfd, buf.data(), buf.size());
	if (result != (signed long long) buf.size()) {
		if (result < 0) {
			throw std::system_error(errno, std::system_category());
		}
		else {
			throw std::system_error(std::make_error_code(std::errc::io_error), "read " + std::to_string(result) + " bytes instead of 36");
		}
	}
}

std::vector<std::unique_ptr<Communicator>> CommunicatorLinux::search() {
	std::vector<std::unique_ptr<Communicator>> result;
	for (auto& dev: std::filesystem::directory_iterator("/dev/")) {
		try {
			if (dev.is_character_file() && !dev.is_symlink() && dev.path().filename().string().find("ttyUSB") == 0) {
				// this might be a Soleux device if:
				// it is a character device
				// it is not a symbolic link
				// it is a USB serial device
				result.push_back(std::make_unique<CommunicatorLinux>(dev.path().string()));
			}
		}
		catch (const not_soleus_error& e) {
			// if it was not a soleus device, ignore it
			// ignore!
		}
		catch (const std::system_error& e) {
			// if there was an i/o error or a permission error, warn the user
			// TODO: how to generate warnings?
			// TEMPORARY SOLUTION
			std::cerr << "Warning: could not read " << dev << ": " << e.what() << '\n';
		}
	}
	return result;
}

void CommunicatorLinux::close() {
	cclose(portfd);
	portfd = -1;
}

std::string CommunicatorLinux::getSerialNumber() {
	return serialNumber;
}

CommunicatorLinux::~CommunicatorLinux() {
	if (portfd != -1) {
		stop();
		close();
	}
}
