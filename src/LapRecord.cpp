#include "LapRecord.hpp"
using namespace soleux;

LapRecord::LapRecord(std::array<char, 36>& buf) : Record(buf) {
	distance = Record::numberFromBytes(buf.begin() + 4, 4, false);
	calories = Record::numberFromBytes(buf.begin() + 8, 4, false);
	maxspeed = (unsigned char) buf[12] * 10 + (unsigned char) buf[13];
	segment = Record::numberFromBytes(buf.begin() + 32, 2, false);
	endtm.tm_isdst = -1;
	endtm.tm_hour = buf[0];
	endtm.tm_min = buf[1];
	endtm.tm_sec = buf[2];
	centisec = buf[3];
}

void LapRecord::updateTime(std::chrono::system_clock::time_point start, std::chrono::system_clock::time_point previous) {
	using std::literals::chrono_literals::operator""h;
	std::time_t start_time_t = std::chrono::system_clock::to_time_t(start);
	std::tm starttm = *std::localtime(&start_time_t);
	std::time_t prev_time_t = std::chrono::system_clock::to_time_t(previous);
	std::tm prevtm = *std::localtime(&prev_time_t);
	endtm.tm_year = prevtm.tm_year;
	endtm.tm_mon  = prevtm.tm_mon;
	endtm.tm_mday = prevtm.tm_mday;
	endtm.tm_hour += starttm.tm_hour; // I don't like this
	endtm.tm_min  += starttm.tm_min;
	endtm.tm_sec  += starttm.tm_sec;
	end = std::chrono::system_clock::from_time_t(std::mktime(&endtm));
	if (end < previous) { // I hate this just as much as you do, but their implementation on the watch leaves me no choice
		end += 24h;
		// endtm.tm_mday++;
		std::time_t end_time_t = std::chrono::system_clock::to_time_t(end);
		std::tm* ndtm = std::localtime(&end_time_t);
		endtm = *ndtm; // copy value
	}
	end += std::chrono::milliseconds(centisec * 10);
}

#ifdef DEBUG
void LapRecord::printDesc(std::ostream& debugLog) {
	Record::printDesc(debugLog);
	debugLog << "LapRecord:\n";
	debugLog << formatDesc(0, 3, "Lap HMSF", std::put_time(&endtm, "%c")) << '\n';
	debugLog << formatDesc(4, 7, "Lap Dist", distance) << '\n';
	debugLog << formatDesc(8, 11, "Lap Cal", calories) << '\n';
	debugLog << formatDesc(12, 13, "Lap Mx.Spd.", maxspeed) << '\n';
	debugLog << formatDesc(32, 33, "Lap Seg", segment) << '\n';
}
#endif
