#include "output.hpp"
#include "Soleux.hpp"

#include <chrono>
#include <iomanip>
#include <sstream>
#include <unordered_map>
#include <regex>
#include <algorithm>
#include <iostream> // for debugging

// this code is kinda messy
// sorry about that
// most of the other files are nice-looking, go look at them instead

int soleux::offset = 0;

// this xml generator doesn't need to be fancy because most of the content is static
// some effort has been spent to make the output xml document human-readable at the expense of file size

std::string iso8601time(std::time_t* t) {
	// some xml parsers can't handle the timezone thing without a colon in it
	// looking at you, xerces
	// so here I am hacking it in manually for you >:(
	std::stringstream result;
	result << std::put_time(std::localtime(t), "%FT%T");
	std::stringstream modify;
	modify << std::put_time(std::localtime(t), "%z");
	std::string modifys = modify.str();
	if (modifys.size() > 2) {
		auto it = modifys.end() - 2;
		modifys.insert(it, ':');
		result << modifys;
	}
	return result.str();
}

void soleux::gpx(std::map<int, GPSRecord>::iterator wpts, std::map<int, GPSRecord>::iterator wptend, std::ostream& out) {
	std::streamsize origPrecision = out.precision();
	out.precision(10);
	if (wpts == wptend) return; // you don't send no waypoints, you don't get no file
	std::time_t local_time_t = std::chrono::system_clock::to_time_t(wpts->second.timestamp);
	out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
	       "<gpx version=\"1.1\" creator=\"soleux https://gitlab.com/danielrparks/soleux\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.topografix.com/GPX/1/1\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">\n"
	       "<metadata>\n"
	       "	<time>" << iso8601time(&local_time_t) << "</time>\n" // ISO8601 time as defined by the xml spec
	       "</metadata>\n"
				 "<trk><trkseg>\n";
	for (; wpts != wptend; wpts++) {
		local_time_t = std::chrono::system_clock::to_time_t(wpts->second.timestamp);
		out << "	<trkpt lat=\"" << wpts->second.latitude << "\" lon=\"" << wpts->second.longitude << "\">\n"
		       "		<time>" << iso8601time(&local_time_t) << "</time>\n"
		       //"	<extensions><hr>" << wpts->second.bpm << "</hr></extensions>\n" // heart rate is temporarily disabled
		       "	</trkpt>\n";
	}
	out << "</trkseg></trk>\n"
	       "</gpx>\n";
	out.precision(origPrecision);
}

auto formatTimeString(std::chrono::system_clock::time_point time) {
	std::time_t temp_time_t = std::chrono::system_clock::to_time_t(time);
	return std::put_time(std::localtime(&temp_time_t), "%Y-%m-%d %H:%M:%S"); // according to stack overflow, this works in excel
}

std::string formatDurationString(std::chrono::system_clock::duration amt) {
	int hours = std::chrono::duration_cast<std::chrono::hours>(amt).count();
	int minutes = std::chrono::duration_cast<std::chrono::minutes>(amt % std::chrono::hours(1)).count();
	int seconds = std::chrono::duration_cast<std::chrono::seconds>(amt % std::chrono::minutes(1)).count();
	int centiseconds = std::chrono::duration_cast<std::chrono::milliseconds>(amt % std::chrono::seconds(1)).count() / 10;
	std::string result("");
	if (hours > 0) result += (hours < 10 ? "0" : "") + std::to_string(hours) + "h";
	if (hours > 0 || minutes > 0) result += (minutes < 10 ? "0" : "") + std::to_string(minutes) + "m";
	result += (seconds < 10 ? "0" : "") + std::to_string(seconds) + ".";
	result += (centiseconds < 10 ? "0" : "") + std::to_string(centiseconds) + "s";
	return result;
}

double formatDistance(long centimeters, bool imperial) {
	if (imperial) {
		return centimeters / 160934.4; // miles
		// fun fact: this is actually more accurate than the soleus sync application, which omits the tenths place in the conversion factor
	}
	else {
		return centimeters / 100000.0; // km
	}
}

std::string formatPace(long centimeters, std::chrono::system_clock::duration dur, bool imperial) {
	//std::chrono::system_clock::duration dur2(dur);
	std::chrono::duration<double, std::chrono::system_clock::period> dur2(std::chrono::duration_cast<std::chrono::duration<double, std::chrono::system_clock::period>>(dur)); // I hate this
	dur2 /= formatDistance(centimeters, imperial);
	return formatDurationString(std::chrono::duration_cast<std::chrono::system_clock::duration>(dur2));
}

std::string formatPace(double speed) {
	if (speed == 0) return "-";
	using std::literals::chrono_literals::operator""h;
	std::chrono::duration<double, std::chrono::system_clock::period> dur2(1h);
	dur2 /= speed;
	return formatDurationString(std::chrono::duration_cast<std::chrono::system_clock::duration>(dur2));
}

double formatCalories(long caloriestimestentothefourth) {
	return caloriestimestentothefourth / 10000.0;
}

double formatMaxSpeed(long maxspeed, bool imperial) {
	if (imperial) {
		return maxspeed / 16.09344;
	}
	else {
		return maxspeed / 10.0;
	}
}

double formatSpeed(long centimeters, std::chrono::system_clock::duration dur, bool imperial) {
	std::chrono::duration<double, std::ratio<3600>> dur2(dur);
	return formatDistance(centimeters, imperial) / dur2.count();
}

std::unordered_map<std::string, soleux::CSVCol> csvmap {
	{"Run #", soleux::CSVCol::runnumber},
	{"Lap #", soleux::CSVCol::lapnumber},
	{"Start Time", soleux::CSVCol::start},
	{"End Time", soleux::CSVCol::end},
	{"Distance", soleux::CSVCol::distance},
	{"Duration", soleux::CSVCol::duration},
	{"Calories", soleux::CSVCol::calories},
	{"Avg. Pace", soleux::CSVCol::avgpace},
	{"Max. Pace", soleux::CSVCol::maxpace},
	{"Avg. Speed", soleux::CSVCol::avgspeed},
	{"Max. Speed", soleux::CSVCol::maxspeed},
	{"Heart Rate", soleux::CSVCol::bpm}
};

std::unordered_map<soleux::CSVCol, std::string> headermap {
	{soleux::CSVCol::runnumber, "Run #"},
	{soleux::CSVCol::lapnumber, "Lap #"},
	{soleux::CSVCol::start, "Start Time"},
	{soleux::CSVCol::end, "End Time"},
	{soleux::CSVCol::distance, "Distance"},
	{soleux::CSVCol::duration, "Duration"},
	{soleux::CSVCol::calories, "Calories"},
	{soleux::CSVCol::avgpace, "Avg. Pace"},
	{soleux::CSVCol::maxpace, "Max. Pace"},
	{soleux::CSVCol::avgspeed, "Avg. Speed"},
	{soleux::CSVCol::maxspeed, "Max. Speed"},
	{soleux::CSVCol::bpm, "Heart Rate"}
};

std::unordered_map<soleux::CSVCol, std::pair<std::string, std::string>> units {
	{soleux::CSVCol::runnumber, {"", ""}},
	{soleux::CSVCol::lapnumber, {"", ""}},
	{soleux::CSVCol::start, {"", ""}},
	{soleux::CSVCol::end, {"", ""}},
	{soleux::CSVCol::distance, {"km", "mi"}},
	{soleux::CSVCol::duration, {"", ""}},
	{soleux::CSVCol::calories, {"kcal", "kcal"}},
	{soleux::CSVCol::avgpace, {"time/km", "time/mi"}},
	{soleux::CSVCol::maxpace, {"time/km", "time/mi"}},
	{soleux::CSVCol::avgspeed, {"km/hr", "mi/hr"}},
	{soleux::CSVCol::maxspeed, {"km/hr", "mi/hr"}},
	{soleux::CSVCol::bpm, {"bpm", "bpm"}}
};

std::regex units_re(R"raw((.*)( \(.*?\)))raw");

std::vector<std::string> soleux::split(std::string str, char delimiter) {
	std::vector<std::string> result;
	unsigned int lastind = 0;
	for (unsigned int i = 0; i < str.size(); i++) {
		if (str[i] == delimiter) {
			result.push_back(str.substr(lastind, i - lastind));
			lastind = i + 1;
		}
	}
	if (lastind != str.size()) {
		result.push_back(str.substr(lastind, str.size() - lastind));
	}
	return result;
}

std::vector<soleux::CSVCol>::size_type soleux::indexOfCol(std::vector<soleux::CSVCol> cols, soleux::CSVCol col) {
	for (std::vector<soleux::CSVCol>::size_type i = 0; i < cols.size(); i++) {
		if (cols[i] == col) return i;
	}
	return -1;
}

std::vector<soleux::CSVCol> soleux::colsFromString(std::string str) {
	std::vector<soleux::CSVCol> result;
	std::vector<std::string> splitted = split(str, ',');
	for (std::string s: splitted) {
		result.push_back(csvmap.at(s));
	}
	return result;
}

// for the char: bit 1: detected, bit 2: true if imperial
std::pair<std::vector<soleux::CSVCol>, char> soleux::colsUnitsFromString(std::string str) {
	std::vector<soleux::CSVCol> result;
	std::vector<std::string> splitted = split(str, ',');
	std::smatch m;
	std::smatch m2;
	char units = 0;
	for (std::string& s: splitted) {
		if (std::regex_match(s, m, units_re)) {
			char thisunits = 0;
			if (m.str(2).find("km") != std::string::npos) {
				thisunits = 1;
			}
			else if (m.str(2).find("mi") != std::string::npos) {
				thisunits = 3;
			}
			if ((units & 1) == 0 && thisunits & 1) {
				units = thisunits;
			}
			s = m[1];
		}
		result.push_back(csvmap.at(s));
	}
	return std::make_pair(result, units);
}

void soleux::formatColumns(std::ostream& out, std::vector<soleux::CSVCol>& columns, bool imperial) {
	for (soleux::CSVCol i: columns) {
		out << headermap.at(i);
		if (imperial) {
			if (units.at(i).second != "") out << " (" << units.at(i).second << ')';
		}
		else {
			if (units.at(i).first != "") out << " (" << units.at(i).first << ')';
		}
		out << ',';
	}
	out << '\n';
}

void formatRunRecord(soleux::RunRecord& run, std::ostream& out, std::vector<soleux::CSVCol>& columns, bool 	imperial) {
	for (soleux::CSVCol i: columns) {
		switch (i) {
			case soleux::CSVCol::runnumber:
				out << run.getNumber() + soleux::offset;
				break;
			case soleux::CSVCol::lapnumber:
				// out << 0;
				out << run.laps.size();
				break;
			case soleux::CSVCol::start:
				out << formatTimeString(run.begin);
				break;
			case soleux::CSVCol::end:
				out << formatTimeString(run.end);
				break;
			case soleux::CSVCol::distance:
				out << formatDistance(run.distance, imperial);
				break;
			case soleux::CSVCol::duration:
				out << formatDurationString(run.end - run.begin);
				break;
			case soleux::CSVCol::calories:
				out << formatCalories(run.calories);
				break;
			case soleux::CSVCol::avgpace:
				out << formatPace(run.distance, run.end - run.begin, imperial);
				break;
			case soleux::CSVCol::maxpace:
				out << formatPace(formatMaxSpeed(run.maxspeed, imperial));
				break;
			case soleux::CSVCol::avgspeed:
				out << formatSpeed(run.distance, run.end - run.begin, imperial);
				break;
			case soleux::CSVCol::maxspeed:
				out << formatMaxSpeed(run.maxspeed, imperial);
				break;
			case soleux::CSVCol::bpm:
				out << run.averageBPM;
				break;
			default:
				break;
		}
		out << ','; // this code actually leaves a comma at the end of every line. This is fine and allowed by the csv format.
	}
	out << '\n';
}

void formatLapRecord(std::map<int, soleux::LapRecord>::iterator& lap, std::ostream& out, int runnumber, std::vector<soleux::CSVCol>& columns, bool 	imperial) {
	for (soleux::CSVCol i: columns) {
		switch (i) {
			case soleux::CSVCol::runnumber:
				out << runnumber + soleux::offset;
				break;
			case soleux::CSVCol::lapnumber:
				out << lap->first;
				break;
			case soleux::CSVCol::start:
				out << formatTimeString(lap->second.begin);
				break;
			case soleux::CSVCol::end:
				out << formatTimeString(lap->second.end);
				break;
			case soleux::CSVCol::distance:
				out << formatDistance(lap->second.distance, imperial);
				break;
			case soleux::CSVCol::duration:
				out << formatDurationString(lap->second.end - lap->second.begin);
				break;
			case soleux::CSVCol::calories:
				out << formatCalories(lap->second.calories);
				break;
			case soleux::CSVCol::avgpace:
				out << formatPace(lap->second.distance, lap->second.end - lap->second.begin, imperial);
				break;
			case soleux::CSVCol::maxpace:
				out << formatPace(formatMaxSpeed(lap->second.maxspeed, imperial));
				break;
			case soleux::CSVCol::avgspeed:
				out << formatSpeed(lap->second.distance, lap->second.end - lap->second.begin, imperial);
				break;
			case soleux::CSVCol::maxspeed:
				out << formatMaxSpeed(lap->second.maxspeed, imperial);
				break;
			case soleux::CSVCol::bpm:
				out << 0;
				break;
			default:
				break;
		}
		out << ',';
	}
	out << '\n';
}

void soleux::csv(std::map<int, RunRecord>::iterator runs, std::map<int, RunRecord>::iterator runend, std::ostream& out, std::vector<soleux::CSVCol>& columns, bool imperial) {
	std::streamsize origPrecision = out.precision();
	out.precision(5);
	for (; runs != runend; runs++) {
		//out << runs->first << ',' << formatTimeString(runs->second.begin) << ',' << formatTimeString(runs->second.end) << ',' << formatDurationString(runs->second.end - runs->second.begin) << ',' << formatDistance(runs->second.distance, imperial) << ',' << formatCalories(runs->second.calories) << ',' << runs->second.averageBPM << '\n';
		formatRunRecord(runs->second, out, columns, imperial);
	}
	out.precision(origPrecision);
}

void soleux::csv(std::map<int, RunRecord>& runs, std::vector<int>& keys, std::ostream& out, std::vector<soleux::CSVCol>& columns, bool imperial) {
	std::streamsize origPrecision = out.precision();
	out.precision(5);
	for (int i: keys) {
		try {
			formatRunRecord(runs.at(i), out, columns, imperial);
		}
		catch (std::out_of_range& e) {
			// ignore this error, it's not an error to me
			continue;
		}
	}
	out.precision(origPrecision);
}

void soleux::csv(std::map<int, LapRecord>::iterator laps, std::map<int, LapRecord>::iterator lapend, int runnumber, std::ostream& out, std::vector<soleux::CSVCol>& columns, bool imperial) {
	std::streamsize origPrecision = out.precision();
	out.precision(5);
	for (; laps != lapend; laps++) {
		//out << runnumber << ',' << laps->first << ',' << formatTimeString(laps->second.begin) << ',' << formatTimeString(laps->second.end) << ',' << formatDurationString(laps->second.end - laps->second.begin) << ',' << formatDistance(laps->second.distance, imperial) << ',' << formatMaxSpeed(laps->second.maxspeed, imperial) << ',' << formatCalories(laps->second.calories) << '\n';
		formatLapRecord(laps, out, runnumber, columns, imperial);
	}
	out.precision(origPrecision);
}
