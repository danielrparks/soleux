#ifndef SOLEUXREC
#define SOLEUXREC

#include <array>
#include <set>
#include <string>
#include <sstream>
#include <ios>
#include <iomanip>
#include "config.hpp"

namespace soleux {
	class Record {
	public:
		enum Type {unset, run, lap, gps, eof}; // this is annoying, but it has to come first
	protected:
		//Type type;
		std::array<char, 36> data;
	public:
		Record(std::array<char, 36>& buf);
		static Type determineType(std::array<char, 36>& buffer);
		int getNumber();
		static long numberFromBytes(std::array<char, 36>::iterator start, int amt, bool isLittleEndian);
		static long transposeBCD(std::array<char, 36>::iterator start, int len);
		static long transposeBCD(std::array<unsigned char, 36>::iterator start, int len);
#ifdef DEBUG
		virtual void printDesc(std::ostream& debugLog);
		template<class value = const char*>
		static std::string formatDesc(int rangeMin, int rangeMax, std::string desc, value val = "") {
			std::stringstream result;
			// [xx, xx]
			// [  xx  ]
			if (rangeMin == rangeMax) {
				result << "[  ";
				result << std::right << std::setw(2) << rangeMin;
				result << "  ]: ";
			} else {
				result << '[';
				result << std::right << std::setw(2) << rangeMin;
				result << ", ";
				result << std::left << std::setw(2) << rangeMax;
				result << "]: ";
			}
			result << std::left << std::setw(12) << desc;
			result << ": ";
			result << val;
			return result.str();
		}
#endif
	};
}

#endif
