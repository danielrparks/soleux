#ifndef SOLEUX
#define SOLEUX
#include "Communicator.hpp"
#include "RunRecord.hpp"
#include "Progress.hpp"
#include "config.hpp"
#include <fstream>

namespace soleux {
	class Soleux {
	private:
		Communicator& connection;
		Progress prog;
		int lastRun;
	public:
		std::map<int, RunRecord> runs; // using a map here just in case the records aren't in order
		Soleux(Communicator& conn);
		void start();
		void stop();
		void close();
		const Progress& readDataBlock();
		const Progress& progress();
		void readAllData();
		void rectify();
		void deleteRun(int runNumber);
		const static int versionMajor = SOLEUX_VERSION_MAJOR;
		const static int versionMinor = SOLEUX_VERSION_MINOR;
		const static int versionPatch = SOLEUX_VERSION_PATCH;
#ifdef DEBUG
	public:
		Soleux(Communicator& conn, std::string bin, std::string log);
	private:
		std::ofstream debugBin, debugLog;
#endif
	};
}
#endif
