#include "Soleux.hpp"
using namespace soleux;

#include <string>
#include <iterator>

std::array<char, 36> buffer;

Soleux::Soleux(Communicator& conn) : connection(conn), lastRun(0) {}

void Soleux::start() {
	prog.clear();
	connection.start();
}

void Soleux::stop() {
	connection.stop();
}

void Soleux::close() {
	connection.close();
#ifdef DEBUG
	debugLog.close();
	debugBin.close();
#endif
}

void Soleux::rectify() {
	for (auto it = runs.begin(); it != runs.end(); it++) {
		it->second.rectify();
	}
}

#ifdef DEBUG
void printDebug(std::ofstream& debugLog, std::string type, soleux::Record& x) {
	if (debugLog.is_open()) {
		debugLog << type << ' ' << x.getNumber() << '\n';
		x.printDesc(debugLog);
		debugLog << '\n';
	}
}
#endif

const Progress& Soleux::readDataBlock() {
	connection.read(buffer);
#ifdef DEBUG
	if (debugLog.is_open()) {
		debugLog << "Offset: " << debugBin.tellp() << ", ";
		debugBin.write(buffer.data(), buffer.size());
	}
#endif
	switch (Record::determineType(buffer)) {
		case Record::Type::run: {
			RunRecord x(buffer);
			// runs[x.getNumber()] = x; // not used because of copy-constructible requirement
			runs.insert({x.getNumber(), x});
			prog.stage = Progress::Stage::runs;
			prog.estimatedPercentage = 0;
			prog.stats.runRecordsProcessed++;
#ifdef DEBUG
			printDebug(debugLog, "RunRecord", x);
#endif
			break;
		}
		case Record::Type::lap: {
			LapRecord x(buffer);
			// runs[x.getNumber()].laps[x.segment] = x;
			runs.at(x.getNumber()).laps.insert({x.segment, x});
			prog.stage = Progress::Stage::laps;
			prog.estimatedPercentage = 0.5; // the gps takes up the vast majority of the space
			prog.stats.lapRecordsProcessed++;
#ifdef DEBUG
			printDebug(debugLog, "LapRecord", x);
#endif
			break;
		}
		case Record::Type::gps: {
			GPSRecord x(buffer);
			// runs[x.getNumber()].wpts[x.segment] = x;
			runs.at(x.getNumber()).wpts.insert({x.segment, x});
			if (prog.stage == Progress::Stage::laps) {
				// we just finished laps! Now we can calculate estimated number of GPS records
				for (auto it1 = runs.begin(); it1 != runs.end(); it1++) {
					auto it2 = it1->second.laps.end();
					it2--;
					prog.stats.totalDistance += it2->second.distance; // this hasn't been rectified yet, so the last lap should have the full distance
				}
				prog.stats.estimatedGPSRecords = prog.stats.totalDistance / Progress::centimetersPerGPSRecord;
				lastRun = x.getNumber();
			}
			prog.stage = Progress::Stage::wpts;
			prog.stats.gpsRecordsProcessed++;
			// calculate estimated percentage
			if (x.getNumber() != lastRun) {
				// we can average the last run now
				auto it = runs.at(lastRun).laps.end();
				it--;
				long lastDistance = it->second.distance;
				long lastGPS = runs.at(lastRun).wpts.size();
				prog.stats.averageCentimetersPerGPSRecord = (prog.stats.averageCentimetersPerGPSRecord * prog.stats.averageSamples + (double) lastDistance / lastGPS) / (prog.stats.averageSamples + 1);
				prog.stats.averageSamples++;
				prog.stats.estimatedGPSRecords = prog.stats.totalDistance / prog.stats.averageCentimetersPerGPSRecord;
			}
			prog.estimatedPercentage = 0.5 + 99.5 * ((double) prog.stats.gpsRecordsProcessed / prog.stats.estimatedGPSRecords);
			if (prog.estimatedPercentage > 100) prog.estimatedPercentage = 100;
#ifdef DEBUG
			printDebug(debugLog, "GPSRecord", x);
#endif
			break;
		}
		case Record::Type::eof:
			prog.stage = Progress::Stage::done;
			prog.estimatedPercentage = 100;
			break;
		default:
			break; // ignore it!
	}
	prog.stats.recordsProcessed++;
	return prog; // TODO: report percentage
}

const Progress& Soleux::progress() {
	return prog;
}

void Soleux::readAllData() {
	while (readDataBlock().stage != Progress::Stage::done); // intended empty loop
}


void Soleux::deleteRun(int runNumber) {
	connection.deleteRun(runNumber);
}

#ifdef DEBUG
Soleux::Soleux(Communicator& conn, std::string bin, std::string log) : Soleux(conn) {
	if (bin.size() > 0) debugBin.open(bin);
	if (log.size() > 0) debugLog.open(log);
}
#endif
