#include "GPSRecord.hpp"
using namespace soleux;

#include <iostream> // for debugging

GPSRecord::GPSRecord(std::array<char, 36>& buf) : Record(buf) {
	segment = Record::numberFromBytes(buf.begin() + 32, 2, false);
	//std::cerr << "seg " << segment << '\n';
	timetm.tm_isdst = -1;
	timetm.tm_hour = buf[8];
	timetm.tm_min = buf[7];
	timetm.tm_sec = buf[6];
	distance = Record::numberFromBytes(buf.begin(), 4, true);
	bpm = buf[10] * 256 + buf[11] / 5; // this is what they did but it might not work
	int latminutes = Record::transposeBCD(buf.begin() + 16, 3);
	int latdegrees = Record::transposeBCD(buf.begin() + 15, 1);
	if (buf[19] == 83) {
		latminutes *= -1;
		latdegrees *= -1;
	}
	latitude = latdegrees + latminutes / 600000.0;
	int lonminutes = Record::transposeBCD(buf.begin() + 22, 3);
	int londegrees = Record::transposeBCD(buf.begin() + 20, 2);
	if (buf[25] == 87) {
		lonminutes *= -1;
		londegrees *= -1;
	}
	longitude = londegrees + lonminutes / 600000.0;
}

void GPSRecord::updateTime(std::chrono::system_clock::time_point start) {
	using std::literals::chrono_literals::operator""h;
	std::time_t start_time_t = std::chrono::system_clock::to_time_t(start);
	std::tm* starttm = std::localtime(&start_time_t);
	timetm.tm_year = starttm->tm_year;
	timetm.tm_mon = starttm->tm_mon;
	timetm.tm_mday = starttm->tm_mday;
	timestamp = std::chrono::system_clock::from_time_t(std::mktime(&timetm));
	if (timestamp < start) { // I hate this just as much as you do, but their implementation on the watch leaves me no choice
		timestamp += 24h;
		//timetm.tm_mday++; // yeah that won't work at the end of the month
		std::time_t timestamp_time_t = std::chrono::system_clock::to_time_t(timestamp);
		std::tm* tstm = std::localtime(&timestamp_time_t);
		timetm = *tstm; // copy value
	}
}

#ifdef DEBUG
void GPSRecord::printDesc(std::ostream& debugLog) {
	Record::printDesc(debugLog);
	debugLog << "GPSRecord:\n";
	debugLog << formatDesc(0, 3, "GPS Dist", distance) << '\n';
	debugLog << formatDesc(6, 8, "GPS SMH", std::put_time(&timetm, "%c")) << '\n';
	debugLog << formatDesc(10, 11, "GPS BPM", bpm) << '\n';
	debugLog << formatDesc(15, 19, "GPS Lat", latitude) << '\n';
	debugLog << formatDesc(20, 25, "GPS Lon", longitude) << '\n';
	debugLog << formatDesc(32, 33, "GPS Seg", segment) << '\n';
}
#endif
