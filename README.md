# soleux

A reverse-engineered implementation of the Soleus Sync protocol for Soleus Running watches meant to run on Linux, written in a real programming language, that exports to local files instead of Strava.

# Extensibility

I develop exclusively on Linux, but I have designed Soleux so that it would be easy to port to other operating systems. Namely, to make it work on Windows, all one would need to do would be to create a Communicator implementation that works on Windows, and then adapt cli.cpp to use it on Windows. (I plan to include CMake support for different operating systems when I am finished.)

__Documentation coming soon!__

# Building

This project depends on [CMake](https://cmake.org/) because Autoconf is too confusing.

```
cd build
cmake ../src/
make
```

# License

    Copyright (C) 2020 Daniel Parks

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

For more information, see [LICENSE](LICENSE).
